# numberGuesser

This app generates a random number between given min and max values.\
In order to win the game, you have to guess said number correctly.

**demo**: https://yolo_dev.gitlab.io/numberguesser/